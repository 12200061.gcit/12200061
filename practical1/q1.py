def powerof_4(num):
    if(num==0):
        return False
    while(num !=1):
        if(num% 4!=0):
            return False
        num>>=2
    return True

print(powerof_4(5))